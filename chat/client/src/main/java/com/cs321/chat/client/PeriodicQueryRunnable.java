package com.cs321.chat.client;

import com.cs321.chat.core.ControlModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ObjectOutputStream;
import java.net.SocketException;

public class PeriodicQueryRunnable implements Runnable {
    private final Logger logger = LoggerFactory.getLogger(PeriodicQueryRunnable.class);
    private GUIModel model;
    private ObjectOutputStream oos;

    public PeriodicQueryRunnable(ObjectOutputStream oos) {
        logger.trace("Instance of " + PeriodicQueryRunnable.class + " created.");
        this.oos = oos;
        model = GUIModel.getInstance();
    }

    @Override
    public void run() {
        try {
            logger.trace("Running " + PeriodicQueryRunnable.class + " now.");

            ControlModel cm = new ControlModel();
            cm.setCommand("list_users");

            oos.writeObject(cm);
            oos.flush();
        }
        catch (SocketException sockEx)
        {
            logger.error("EOFException in MessageReceivedHandler: " + sockEx.getMessage());
        }
        catch (Exception e) {
            logger.error(e.getMessage());
        }
    }
}
