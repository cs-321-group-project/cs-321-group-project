package com.cs321.chat.client;

import com.cs321.chat.core.ControlModel;
import com.cs321.chat.core.interfaces.IMessageModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.EOFException;
import java.io.ObjectInputStream;
import java.net.SocketException;

public class MessageReceivedHandler implements Runnable {
    private final Logger logger = LoggerFactory.getLogger(MessageReceivedHandler.class);
    private GUIModel model;
    private ObjectInputStream ois;
    private MainWindow mainWindow;

    public MessageReceivedHandler(ObjectInputStream ois, MainWindow mainWindow) {
        logger.debug("Instance of " + MessageReceivedHandler.class + " created.");
        this.model = GUIModel.getInstance();
        this.ois = ois;
        this.mainWindow = mainWindow;
    }

    @Override
    public void run() {
        try {
            Object obj = ois.readObject(); //Blocking call

            while(obj != null)
            {
                if (obj instanceof IMessageModel) {
                    IMessageModel mm = (IMessageModel)obj;
                    logger.debug("Received message: " + mm);

                    mainWindow.addMessageToUserTab(mm.getUsername(), mm.getUsername(), mm.getMsg());
                }
                else if (obj instanceof ControlModel) {
                    ControlModel cm = (ControlModel)obj;
                    logger.debug("Received control response: '" + cm.getCommandResult() + "' for command: '" + cm.getCommand() + "'");

                    String command = cm.getCommand();
                    switch(command) {
                        case "list_users": {
                            model.setCurrentlyConnectedUsers(cm.getCommandResult().get(command).toArray(new String[0])); //Don't even ask why converting a List<String> to String[] is that ugly, but it is
                            mainWindow.updateConnectedUsersList();
                            break;
                        }

                        default: {
                            logger.error("Command: '{}' is not recognised.", command);
                            break;
                        }
                    }
                }

                obj = ois.readObject(); //Blocking call
            }

        }
        catch (EOFException eofEx)
        {
            logger.error("EOFException in MessageReceivedHandler: " + eofEx.getMessage());
            if(!model.getSocketClient().isClosed())
                try { model.getSocketClient().close(); } catch (Exception e) { logger.error("Socket failed to close: " + e.getMessage()); }
            mainWindow.updateGUIConnectionState();
        }
        catch (SocketException sockEx)
        {
            logger.error("SocketException in MessageReceivedHandler: " + sockEx.getMessage());
            if(!model.getSocketClient().isClosed())
                try { model.getSocketClient().close(); } catch (Exception e) { logger.error("Socket failed to close: " + e.getMessage()); }
            mainWindow.updateGUIConnectionState();

        }
        catch(Exception e) {
            e.printStackTrace();
            logger.error("Error in MessageReceivedHandler: " + e.getMessage());
        }
    }
}
