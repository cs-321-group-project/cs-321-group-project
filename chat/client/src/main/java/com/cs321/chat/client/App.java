package com.cs321.chat.client;

import javax.swing.*;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) {
        try
        {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

            MainWindow m = new MainWindow();
            m.setVisible(true);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
