package com.cs321.chat.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.Socket;

public class GUIModel {
    private Logger logger = LoggerFactory.getLogger(GUIModel.class);
    private static GUIModel ourInstance = new GUIModel();

    private Socket socketClient;
    private String[] currentlyConnectedUsers;
    private String ipAddress;
    private int port;
    private String userName;

    private GUIModel() {
        logger.debug("Instantiating the GUI GUIModel.");
    }

    public static GUIModel getInstance()
    {
        return ourInstance;
    }

    public Socket getSocketClient() {
        return socketClient;
    }

    public void setSocketClient(Socket socketClient) {
        this.socketClient = socketClient;
    }

    public String[] getCurrentlyConnectedUsers() {
        return currentlyConnectedUsers;
    }

    public void setCurrentlyConnectedUsers(String[] currentlyConnectedUsers) {
        this.currentlyConnectedUsers = currentlyConnectedUsers;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

}
