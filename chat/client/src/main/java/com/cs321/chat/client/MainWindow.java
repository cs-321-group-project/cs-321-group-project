package com.cs321.chat.client;

import com.cs321.chat.core.CheckinModel;
import com.cs321.chat.core.MessageModel;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.text.DefaultCaret;
import java.awt.*;
import java.awt.event.*;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.*;

public class MainWindow extends JFrame
{
    private JPanel mainPanel;
    private JToolBar toolBar;
    private JTextArea chatTextArea;
    private JTextField portTextField;
    private JTextField ipTextField;
    private JTextField usernameTextField;
    private JLabel ipLabel;
    private JLabel portLabel;
    private JLabel usernameLabel;
    private JButton connectButton;
    private JLabel connectionStatusLabel;
    private JButton sendButton;
    private JTextField messageTextField;
    private JList connectedUsersList;
    private JTabbedPane activeUserChats;

    private Logger logger = LoggerFactory.getLogger(GUIModel.class);
    private GUIModel model;
    private ObjectOutputStream oos;
    private ObjectInputStream ois;

    private ScheduledExecutorService scheduledExecutorService;
    private ThreadFactory threadFactory;

    private MessageReceivedHandler messageReceivedHandler;
    private PeriodicQueryRunnable periodicQueryRunnable;

    private Future<?> mrhFuture;
    private Future<?> pqrFuture;

    public MainWindow() {
        // This uses the mainPanel we designed in the GUI editor
        add(mainPanel);

        this.model = GUIModel.getInstance();

        this.threadFactory = new ThreadFactoryBuilder()
                .setNameFormat("cli-updt-thr-%d")
                .build();

        this.scheduledExecutorService = Executors.newScheduledThreadPool(2, this.threadFactory);

        this.setTitle("CS321 Chat Application");
        this.setSize(800,600);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        updateGUIConnectionState(); //Initially set the state of everything

        connectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(model.getSocketClient() == null || model.getSocketClient().isClosed()) {
                    try {
                        if (ipTextField.getText().isEmpty() || portTextField.getText().isEmpty() || usernameTextField.getText().isEmpty()) //If any of the fields are empty, do nothing
                            return;

                        logger.trace("Attempting to connect to server...");

                        //Updating the model with the data that the user entered
                        model.setIpAddress(ipTextField.getText());
                        model.setPort(Integer.parseInt(portTextField.getText()));
                        model.setUserName(usernameTextField.getText());

                        //Creating a new socket with the IP address and port with a timeout of 3 seconds.
                        Socket sock = new Socket();
                        sock.connect(new InetSocketAddress(model.getIpAddress(), model.getPort()), 3000);
                        model.setSocketClient(sock);

                        if (model.getSocketClient().isConnected()) { //If we're here, we're hopefully connected, but lets double check
                            oos = new ObjectOutputStream(sock.getOutputStream());
                            ois = new ObjectInputStream(sock.getInputStream());

                            //Once we're connected, we have to send a CheckinModel with our username that we want the server to associate our connection with
                            CheckinModel cm = new CheckinModel(model.getUserName());

                            oos.writeObject(cm);

                            periodicQueryRunnable = new PeriodicQueryRunnable(oos);
                            messageReceivedHandler = new MessageReceivedHandler(ois, MainWindow.this);

                            //Creating scheduled runnables that periodically check for received messages and ping the server for active users that are connected
                            mrhFuture = scheduledExecutorService.scheduleAtFixedRate(messageReceivedHandler, 1, 2, TimeUnit.SECONDS);
                            pqrFuture = scheduledExecutorService.scheduleAtFixedRate(periodicQueryRunnable, 0, 5, TimeUnit.SECONDS);
                        }
                    }
                    catch (UnknownHostException e) {
                        logger.warn("Invalid hostname: " + e.getMessage());
                    }
                    catch (SocketTimeoutException e) {
                        logger.warn("Tried connecting to host, but timed out: " + e.getMessage());
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                        logger.error(e.getMessage());
                    }
                }
                else {
                    try {
                        logger.trace("Attempting to disconnect from server...");
                        pqrFuture.cancel(true);
                        mrhFuture.cancel(true);
                    }
                    catch(Exception e) {
                        e.printStackTrace();
                        logger.error(e.getMessage());
                    }
                    finally {
                        if (oos != null)
                            try {  oos.close(); } catch (Exception e) { }
                        if (ois != null)
                            try { ois.close(); } catch (Exception e) { }
                        if(!model.getSocketClient().isClosed())
                            try { model.getSocketClient().close(); } catch (Exception e) { logger.error("Socket failed to close: " + e.getMessage()); }
                    }
                }
                updateGUIConnectionState(); //Updates the GUI stuff to appear either connected or disconnected
            }
        });

        //SendButton and messageTextField actionListeners should be the same. The only difference is the method in which they are called (sendButton via a click, and messageTextField from the Enter key being pressed)
        sendButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(messageTextField.getText().isEmpty() || getActiveTabName().equalsIgnoreCase("welcome")) //Ignore blank messages and the welcome tab
                    return;

                //Adding our message to the UI, sending the message over the network, and clearing the messageTextField
                addMessageToUserTab(getActiveTabName(), model.getUserName(), messageTextField.getText());
                sendMessage(getActiveTabName(), messageTextField.getText());
                messageTextField.setText("");
            }
        });

        //SendButton and messageTextField actionListeners should be the same. The only difference is the method in which they are called (sendButton via a click, and messageTextField from the Enter key being pressed)
        messageTextField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(messageTextField.getText().isEmpty() || getActiveTabName().equalsIgnoreCase("welcome")) //Ignore blank messages and the welcome tab
                    return;

                //Adding our message to the UI, sending the message over the network, and clearing the messageTextField
                addMessageToUserTab(getActiveTabName(), model.getUserName(), messageTextField.getText());
                sendMessage(getActiveTabName(), messageTextField.getText());
                messageTextField.setText("");
            }
        });

        //This listener listens to mouse events that happen on the connectedUsersList
        connectedUsersList.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                    super.mouseClicked(e);
                    JList list = (JList) e.getSource();

                    if (e.getClickCount() == 2) { //If the number of clicks is 2, then try adding the clicked user to a new tab
                        // Double-click detected
                        int index = list.locationToIndex(e.getPoint());
                        logger.trace("Double click index: " + index);

                        if(index == -1) //If the index is -1, then it is because the list was empty
                            return;

                        String user = list.getModel().getElementAt(index).toString();
                        if (!userExistsInTabPane(user)) { //If the user doesn't already exist in the tab pane, then create a new tab for them
                            addTabForUser(user);
                        }
                    }
                }
                catch (IndexOutOfBoundsException err) {
                    logger.error(err.getMessage());
                }
                catch (Exception err) {
                    logger.error(err.getMessage());
                }
            }
        });

        //This listener listens to mouse events on the activeUserChats tab pane
        activeUserChats.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                JTabbedPane tabbedPane = (JTabbedPane)e.getSource();

                if(e.getClickCount() == 1) { //If the number of clicks was 1, then we assume that they viewed the tab and set the color of the tab back to white
                    int index = tabbedPane.getSelectedIndex();
                    if(tabbedPane.isEnabled() && index != 0) {
                        activeUserChats.setBackgroundAt(index, new Color(255,255,255));
                    }
                }
                else if (e.getClickCount() == 2) { //If the number of clicks was 2, then remove the tab
                    // Double-click detected
                    int index = tabbedPane.getSelectedIndex();
                    logger.trace("Double click index: " + index);

                    if(tabbedPane.isEnabled()) {
                        tabbedPane.removeTabAt(index);
                    }
                }
            }
        });
    }

    /**
     * Helper method to update the GUI look and feel based upon the current server connection status.
     */
    public void updateGUIConnectionState() {
        if(model.getSocketClient() == null || model.getSocketClient().isClosed()) {
            logger.debug("Setting GUI state to: Not Connected (false)");
            connectButton.setText("Connect");
            activeUserChats.setEnabled(false);
            messageTextField.setEnabled(false);
            sendButton.setEnabled(false);
            connectionStatusLabel.setIcon(new ImageIcon(App.class.getResource("/Icons/ConnectionBadRound.png")));
            connectionStatusLabel.setText("Disconnected");
            portTextField.setEnabled(true);
            ipTextField.setEnabled(true);
            usernameTextField.setEnabled(true);

            if(mrhFuture != null && !mrhFuture.isCancelled())
                mrhFuture.cancel(true);

            if(pqrFuture != null && !pqrFuture.isCancelled())
                pqrFuture.cancel(true);
        }
        else {
            logger.debug("Setting GUI state to: Connected (true)");
            connectButton.setText("Disconnect");
            activeUserChats.setEnabled(true);
            messageTextField.setEnabled(true);
            sendButton.setEnabled(true);
            connectionStatusLabel.setIcon(new ImageIcon(App.class.getResource("/Icons/ConnectionGoodRound.png")));
            connectionStatusLabel.setText("Connected");
            portTextField.setEnabled(false);
            ipTextField.setEnabled(false);
            usernameTextField.setEnabled(false);
        }
    }

    /**
     * Helper method that sends a message to the server with the specified user 'user' with the given message 'msg'.
     * @param user
     * @param msg
     */
    private void sendMessage(String user, String msg) {
        try {
            MessageModel mm = new MessageModel();
            mm.setUsername(model.getUserName());
            mm.setDestination(user);
            mm.setMsg(msg);

            logger.debug("Sending message: " + mm.toString());

            oos.writeObject(mm);
            oos.flush();
        }
        catch(Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
    }

    /**
     * Helper function to check if a user exists within the tab pane
     * @param user
     * @return boolean of if the user exists
     */
    private boolean userExistsInTabPane(String user) {
        return (activeUserChats.indexOfTab(user) != -1);
    }

    /**
     * Creates a tab and adds it to the tab pane with the given param 'user'. This method also
     * adds a JScrollArea within the tab so that the user can scroll up or down if the data
     * contained within exceeds the height of the window.
     * @param user
     */
    private void addTabForUser(String user) {
        JTextArea textArea = new JTextArea();
        textArea.setEditable(false);
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);
        DefaultCaret caret = (DefaultCaret) textArea.getCaret(); //Makes the textArea auto-scroll downwards
        caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);

        JScrollPane scrollPane = new JScrollPane(textArea);

        activeUserChats.addTab(user, scrollPane);
//        activeUserChats.setSelectedIndex(activeUserChats.getTabCount()-1); //This reselects the current tab to the one that was just created
        activeUserChats.setBackgroundAt(activeUserChats.getTabCount()-1, new Color(255,255,255));
    }

    /**
     * Returns the name of the tab that is currently being viewed
     * @return name of tab
     */
    public String getActiveTabName() {
        return activeUserChats.getTitleAt(activeUserChats.getSelectedIndex());
    }

    /**
     * Updates the list of active users to what is currently stored in the model.
     */
    public void updateConnectedUsersList() {
        connectedUsersList.setListData(model.getCurrentlyConnectedUsers());
    }

    /**
     * Adds a message 'msg' to the specified user's tab 'toUser' with the username it is from 'fromUser'
     * If the tab does not exist in the user interface, then a new tab will be created.
     * @param toUser
     * @param fromUser
     * @param msg
     */
    public void addMessageToUserTab(String toUser, String fromUser, String msg) {
        if(!userExistsInTabPane(toUser)) //If the user has a tab that exists for them, then plop that message right on in there
            addTabForUser(toUser);

        int indexOfUser = activeUserChats.indexOfTab(toUser);

        JTextArea textArea = (JTextArea) ((JScrollPane) activeUserChats.getComponentAt(indexOfUser)).getViewport().getView();
        textArea.append("[" + new SimpleDateFormat("HH:mm:ss").format(new Date()) + "] " + fromUser + ": " + msg + "\n");

        if(activeUserChats.getSelectedIndex() != indexOfUser)
            activeUserChats.setBackgroundAt(indexOfUser, new Color(255, 255, 0));
    }
}
