package com.cs321.chat.client;

import org.junit.BeforeClass;
import org.junit.Test;

import java.net.Socket;

import static org.junit.Assert.*;

public class GUIModelTest {

    private static GUIModel guiModel;

    @BeforeClass
    public static void init()
    {
        guiModel = GUIModel.getInstance(); // Construct the global instance.
    }

    @Test
    public void getInstance() {
        System.out.println("--- Testing singleton ---");
        GUIModel m = GUIModel.getInstance();

        if (m != guiModel) // If the addresses aren't the same. Its not a singleton.
            fail("The instances are not the same. This is not a singleton.");

        if (!m.equals(guiModel)) // " "
            fail("The classes are not the same.");
    }
}