/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs321.chat.core.exceptions;

/**
 *
 * @author haleydelmas
 */
public class InvalidPortException extends Exception {

    /**
     * Creates a new instance of <code>InvalidPortException</code> without
     * detail message.
     */
    public InvalidPortException() {
    }

    /**
     * Constructs an instance of <code>InvalidPortException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public InvalidPortException(String msg) {
        super(msg);
    }
}
