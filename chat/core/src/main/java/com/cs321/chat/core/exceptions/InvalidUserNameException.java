/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs321.chat.core.exceptions;

/**
 *
 * @author haleydelmas
 */
public class InvalidUserNameException extends Exception {

    /**
     * Creates a new instance of <code>InvalidUserNameException</code> without
     * detail message.
     */
    public InvalidUserNameException() {
    }

    /**
     * Constructs an instance of <code>InvalidUserNameException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public InvalidUserNameException(String msg) {
        super(msg);
    }
}
