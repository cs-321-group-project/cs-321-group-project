package com.cs321.chat.core.interfaces;

import java.util.List;
import java.util.Map;

public interface IControlModel
{
    public String getCommand();
    public Map<String, List<String>> getCommandResult();
}
