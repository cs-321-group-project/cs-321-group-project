package com.cs321.chat.core.interfaces;

public interface IMessageModel
{
    public String getUsername();
    public String getDestination();
    public String getMsg();
}
