/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs321.chat.core.exceptions;

/**
 *
 * @author haleydelmas
 */
public class InvalidIPException extends Exception {

    /**
     * Creates a new instance of <code>InvalidIPException</code> without detail
     * message.
     */
    public InvalidIPException() {
    }

    /**
     * Constructs an instance of <code>InvalidIPException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public InvalidIPException(String msg) {
        super(msg);
    }
}
