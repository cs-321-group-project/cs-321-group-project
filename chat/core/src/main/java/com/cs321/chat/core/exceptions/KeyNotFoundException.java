/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs321.chat.core.exceptions;

/**
 *
 * @author haleydelmas
 */
public class KeyNotFoundException extends Exception {

    /**
     * Creates a new instance of <code>KeyNotFoundException</code> without
     * detail message.
     */
    public KeyNotFoundException() {
    }

    /**
     * Constructs an instance of <code>KeyNotFoundException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public KeyNotFoundException(String msg) {
        super(msg);
    }
}
