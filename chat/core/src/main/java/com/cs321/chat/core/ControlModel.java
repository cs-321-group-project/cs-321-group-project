package com.cs321.chat.core;

import com.cs321.chat.core.interfaces.IControlModel;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ControlModel implements IControlModel, Serializable
{
    private String command;

    private Map<String, List<String>> commandResult;
    /**
     * calls initialize 
     */
    public ControlModel()
    {
        this.init();
    }
    /**
     * calls initialize and sets private string command 
     * @param command 
     */
    public ControlModel(String command)
    {
        this.init();
        this.command = command;
    }
    /**
     * initialize command to null and set commandResult to a new Hashmap
     */
    private void init()
    {
        this.command = null;
        this.commandResult = new HashMap<>();
    }
    /**
     * returns command string
     * @return 
     */
    public String getCommand()
    {
        return command;
    }
    /**
     * sets the command
     * @param command 
     */
    public void setCommand(String command)
    {
        this.command = command;
    }
    /**
     * returns the commandResult
     * @return 
     */
    public Map<String, List<String>> getCommandResult()
    {
        return commandResult;
    }
    /**
     * puts string command and the list into the commandResult
     * @param command
     * @param out 
     */
    public void setCommandResult(String command, List<String> out)
    {
        this.commandResult.put(command, out);
    }
}
