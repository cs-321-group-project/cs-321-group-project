/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs321.chat.core.exceptions;

/**
 *
 * @author haleydelmas
 */
public class InvalidMessageException extends Exception {

    /**
     * Creates a new instance of <code>InvalidMessageException</code> without
     * detail message.
     */
    public InvalidMessageException() {
    }

    /**
     * Constructs an instance of <code>InvalidMessageException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public InvalidMessageException(String msg) {
        super(msg);
    }
}
