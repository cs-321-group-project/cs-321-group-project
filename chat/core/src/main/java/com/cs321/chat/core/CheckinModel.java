package com.cs321.chat.core;

import com.cs321.chat.core.interfaces.ICheckinModel;

import java.io.Serializable;

public class CheckinModel implements ICheckinModel, Serializable
{
    private String username;
    /**
     * Sets the username
     * @param username 
     */
    public CheckinModel(String username)
    {
        this.username = username;
    }
    /**
     * Will return the username
     * @return 
     */
    public String getUsername()
    {
        return username;
    }
}
