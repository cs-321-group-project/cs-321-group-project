package com.cs321.chat.core;

import com.cs321.chat.core.interfaces.IMessageModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

public class MessageModel implements IMessageModel, Serializable
{
    private final Logger logger = LoggerFactory.getLogger(MessageModel.class);
    private String msg;
    private String username;
    private String destination;
    /**
     * prints in logger the construction of model
     */
    public MessageModel()
    {
        logger.trace("Construction of message model.");
    }
    /**
     * returns the msg string
     * @return 
     */
    public String getMsg()
    {
        return msg;
    }
    /**
     * sets the message to private string message
     * @param msg 
     */
    public void setMsg(String msg)
    {
        this.msg = msg;
    }
    /**
     * returns the username
     * @return 
     */
    public String getUsername()
    {
        return username;
    }
    /**
     * sets username of string
     * @param username 
     */
    public void setUsername(String username)
    {
        this.username = username;
    }
    /**
     * returns the destination 
     * @return 
     */
    public String getDestination()
    {
        return destination;
    }
    /**
     * sets destinations 
     * @param destination 
     */
    public void setDestination(String destination)
    {
        this.destination = destination;
    }
    /**
     * prints the message , username, and destination of message sent
     * @return 
     */
    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("\n\tMessage: " + this.msg);
        sb.append("\n\tUsername: " + this.username);
        sb.append("\n\tDestination: " + this.destination);

        return sb.toString();
    }
}
