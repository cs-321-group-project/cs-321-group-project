package com.cs321.chat.server;

import com.cs321.chat.core.CheckinModel;
import com.cs321.chat.core.exceptions.UsernameExistsException;
import com.cs321.chat.core.interfaces.IMessageModel;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Queue;

import static org.junit.Assert.fail;

public class ThreadSafeModelTest
{
    private final int ranLength = 15;
    private static ThreadSafeModel threadSafeModel;

    @BeforeClass
    public static void init()
    {
        threadSafeModel = ThreadSafeModel.getInstance(); // Construct the global instance.
    }

    @Test
    public void singletonTest()
    {
        System.out.println("--- Testing singleton ---");
        ThreadSafeModel tsm = ThreadSafeModel.getInstance();

        if (tsm != threadSafeModel) // If the addresses aren't the same. Its not a singleton.
            fail("The instances are not the same. This is not a singleton.");

        if (!tsm.equals(threadSafeModel)) // " "
            fail("The classes are not the same.");
    }

    @Test
    public void setGetMsgTest()
    {
        System.out.println("--- Testing gets / sets ---");

        try
        {
            IMessageModel imm = TestUtil.generateRandomMessage();
            threadSafeModel.addMessage(imm);

            System.out.println("First message: \n" + imm);

            IMessageModel imm1 = threadSafeModel.getMessage(imm.getDestination());

            System.out.println("Second messsage: \n" + imm1);

            if (imm != imm1 || !imm.equals(imm1))
                fail("The get / set message was not the same.");
        }
        catch (Exception e)
        {
            e.printStackTrace();
            fail("Caught exception: " + e.getMessage());
        }
    }

    @Test
    public void setGetQueueTest()
    {
        System.out.println("--- Testing message queue ---");

        try
        {
            for (int i = 0; i < 100; i++)
                threadSafeModel.addMessage(TestUtil.generateRandomMessageWithUsername("oofta"));

            Queue<IMessageModel> tempQ = threadSafeModel.getMessageQueue("oofta");

            if (tempQ.size() != 100)
                fail("We lost a message!");

            if (tempQ.size() != threadSafeModel.getMessageQueueSize("oofta"))
                fail("We have the size incorrect in the model.");

            for (int i = 0; i < 100; i++)
                threadSafeModel.getMessage("oofta");

            Queue<IMessageModel> tempQ1 = threadSafeModel.getMessageQueue("oofta");

            if (tempQ1.size() != 0)
                fail("We didn't pop the messages.");

            if (tempQ1.size() != threadSafeModel.getMessageQueueSize("oofta"))
                fail("We have incorrect data size in model.");
        }
        catch (Exception e)
        {
            e.printStackTrace();
            fail("Caught exception: " + e.getMessage());
        }
    }

    @Test
    public void noDuplicateUserTest()
    {
        System.out.println("--- Testing for user duplication ---");

        boolean isThrown = false;
        try
        {
            CheckinModel cm1 = new CheckinModel("dup_user");
            CheckinModel cm2 = new CheckinModel("dup_user");
            threadSafeModel.checkIn(cm1); // should be ok
            threadSafeModel.checkIn(cm2); // should throw exception
        }
        catch (UsernameExistsException ex)
        {
            isThrown = true;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            fail("Unhandled exception: " + ex.getMessage());
        }
        if (isThrown) {
            System.out.println("good handling");
            return;
        }
        fail("Exception was expected but not thrown.");
    }
}
