package com.cs321.chat.server;

import static org.junit.Assert.fail;

import com.cs321.chat.core.CheckinModel;
import com.cs321.chat.core.ControlModel;
import com.cs321.chat.core.MessageModel;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import java.io.*;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    private final Logger logger = LoggerFactory.getLogger(AppTest.class);

    @Test
    public void test1()
    {
        System.out.println("============ TEST 1 ===============");
        try
        {
            ThreadFactory threadFactory = new ThreadFactoryBuilder()
                    .setNameFormat("testing-thr-%d")
                    .build();

            ExecutorService executorService = Executors.newFixedThreadPool(2,
                    threadFactory);

            TestUtil.TestRunner tr = new TestUtil.TestRunner();
            TestClient tc = new TestClient();
            executorService.submit(tr);
            Thread.sleep(1000);
            executorService.submit(tc);
            Thread.sleep(6000);
        }
        catch (Exception ex)
        {
            fail(ex.getMessage());
        }
    }
    @Test
    public void test2()
    {
        System.out.println("============ TEST 2 ===============");
        try
        {
            ThreadFactory threadFactory = new ThreadFactoryBuilder()
                    .setNameFormat("testing-thr-%d")
                    .build();

            ExecutorService executorService = Executors.newFixedThreadPool(2,
                    threadFactory);

            TestUtil.TestRunner tr = new TestUtil.TestRunner();
            TestClient tc1 = new TestClient();
            TestClient2 tc2 = new TestClient2();
            executorService.submit(tr);
            //Thread.sleep(1000);
            executorService.submit(tc1);
            //Thread.sleep(1000);
            executorService.submit(tc2);
            Thread.sleep(6000);
        }
        catch (Exception ex)
        {
            fail(ex.getMessage());
        }
    }
    @Test
    public void test3()
    {
        System.out.println("============ TEST 3 ===============");
        try
        {
            ThreadFactory threadFactory = new ThreadFactoryBuilder()
                    .setNameFormat("testing-thr-%d")
                    .build();

            ExecutorService executorService = Executors.newFixedThreadPool(2,
                    threadFactory);

            TestUtil.TestRunner tr = new TestUtil.TestRunner();
            Dup d1 = new Dup();
            Dup d2 = new Dup();
            executorService.submit(tr);
            executorService.submit(d1);
            executorService.submit(d2);
            Thread.sleep(6000);
        }
        catch (Exception ex)
        {
            fail(ex.getMessage());
        }
    }

    // Utility for test:

    private class TestClient implements Runnable
    {
        @Override
        public void run()
        {
            try
            {
                CheckinModel cm = new CheckinModel("testmn");
                MessageModel mm = new MessageModel();
                ControlModel controlModel = new ControlModel();
                controlModel.setCommand("list_users");
                MessageModel mm1 = new MessageModel();
                MessageModel mm2 = new MessageModel();

                Socket socketClient= new Socket("localhost",5678);

                mm.setUsername("testmn");
                mm.setDestination("oof");
                mm.setMsg("I hate living :(");

                mm1.setUsername("testmn");
                mm1.setDestination("oof");
                mm1.setMsg("me too bro..");

                mm2.setUsername("testmn");
                mm2.setDestination("oof");
                mm2.setMsg("k bye now!");

                ObjectOutputStream oos = new ObjectOutputStream(socketClient.getOutputStream());
                oos.writeObject(cm);
                oos.flush();
                oos.writeObject(controlModel);
                oos.flush();
                oos.writeObject(mm);
                oos.flush();
                oos.writeObject(mm1);
                oos.flush();
                oos.writeObject(mm2);
                oos.flush();

                socketClient.close(); // im done
            }
            catch (Exception ex)
            {
                fail("Caught exception: " + ex.getMessage());
            }
        }
    }
    private class TestClient2 implements Runnable
    {
        @Override
        public void run()
        {
            try
            {
                //CheckinModel cm = new CheckinModel("testmn");
                CheckinModel cm1 = new CheckinModel("oof");
                MessageModel mm = new MessageModel();
                MessageModel mm1 = new MessageModel();
                MessageModel mm2 = new MessageModel();

                Socket socketClient = new Socket("localhost",5678);

                mm.setUsername("oof");
                mm.setDestination("testmn");
                mm.setMsg("I hate living :(");

                mm1.setUsername("oof");
                mm1.setDestination("testmn");
                mm1.setMsg("me too bro..");

                mm2.setUsername("oof");
                mm2.setDestination("testmn");
                mm2.setMsg("k bye now!");

                ObjectOutputStream oos = new ObjectOutputStream(socketClient.getOutputStream());

                oos.writeObject(cm1);
                oos.flush();
                oos.writeObject(mm);
                oos.flush();
                oos.writeObject(mm1);
                oos.flush();
                oos.writeObject(mm2);
                oos.flush();

                socketClient.close(); // im done
            }
            catch (Exception ex)
            {
                fail("Caught exception: " + ex.getMessage());
            }
        }
    }
    private class Dup implements Runnable
    {
        @Override
        public void run() {
            try
            {
                CheckinModel cm1 = new CheckinModel("oof_dup");
                Socket socketClient = new Socket("localhost",5678);
                ObjectOutputStream oos = new ObjectOutputStream(socketClient.getOutputStream());
                oos.writeObject(cm1);
                oos.flush();
            }
            catch (Exception ex)
            {
                fail("Caught exception: " + ex.getMessage());
            }
        }
    }
}
