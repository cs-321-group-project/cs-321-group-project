package com.cs321.chat.server;

import com.cs321.chat.core.ControlModel;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.fail;

public class ClientCommandHandlerTest
{
    private final int ranLength = 15;
    private static ThreadSafeModel threadSafeModel;

    @BeforeClass
    public static void init()
    {
        threadSafeModel = ThreadSafeModel.getInstance(); // Construct the global instance.
    }

    @Test
    public void test1()
    {
        System.out.println("--- Testing command 'list_users' ---");
        try
        {
            for (int i = 0; i < 20; i++)
                threadSafeModel.checkIn(TestUtil.generateRandomCheckinModel());

            ControlModel cm = new ControlModel("list_users");
            ClientCommandHandler cch = new ClientCommandHandler(cm);
            ControlModel n = cch.parseAndFill();
            List<String> oof = n.getCommandResult().get("list_users");

            System.out.println(Arrays.toString(oof.toArray()));
        }
        catch (Exception e)
        {
            e.printStackTrace();
            fail("Caught exception: " + e.getMessage());
        }
    }
}
