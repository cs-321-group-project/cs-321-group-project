package com.cs321.chat.server;

import com.cs321.chat.core.CheckinModel;
import com.cs321.chat.core.MessageModel;
import com.cs321.chat.core.interfaces.ICheckinModel;
import com.cs321.chat.core.interfaces.IMessageModel;
import org.apache.commons.lang3.RandomStringUtils;

import static org.junit.Assert.fail;

public class TestUtil
{
    private static int ranLength = 15;
    public static IMessageModel generateRandomMessage()
    {
        MessageModel mm = new MessageModel();

        mm.setDestination(RandomStringUtils.randomAlphanumeric(ranLength));
        mm.setMsg(RandomStringUtils.randomAlphanumeric(ranLength));
        mm.setUsername(RandomStringUtils.randomAlphanumeric(ranLength));

        return mm;
    }
    public static IMessageModel generateRandomMessageWithUsername(String uname)
    {
        MessageModel mm = new MessageModel();

        mm.setDestination(uname);
        mm.setMsg(RandomStringUtils.randomAlphanumeric(ranLength));
        mm.setUsername(RandomStringUtils.randomAlphanumeric(ranLength));

        return mm;
    }
    public static ICheckinModel generateRandomCheckinModel()
    {
        CheckinModel cm = new CheckinModel(RandomStringUtils.randomAlphanumeric(ranLength));
        return cm;
    }
    public static class TestRunner implements Runnable
    {
        @Override
        public void run()
        {
            try
            {
                ChatServer cs = new ChatServer();
                cs.setNumThreads(2);
                cs.setPort(5678);
                cs.run();
            }
            catch (Exception ex)
            {
                fail("Caught exception: " + ex.getMessage());
            }
        }
    }
}
