package com.cs321.chat.server;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

public class ChatServer
{
    private final Logger logger = LoggerFactory.getLogger(ChatServer.class);

    private ServerSocket serverSocket;
    private ThreadFactory threadFactory;
    private ExecutorService executorService;
    private int numThreads;
    private int port;

    /**
     * Default constructor for the chat server.
     */
    public ChatServer()
    {
        this.init();
    }

    private void init()
    {
        logger.debug("Initializing chat server.");
        this.serverSocket = null;
        this.numThreads = Runtime.getRuntime().availableProcessors();

        this.threadFactory = new ThreadFactoryBuilder()
                .setNameFormat("srv-msg-thr-%d")
                .setDaemon(true)
                .build();

        this.executorService = Executors.newFixedThreadPool(this.numThreads,
                this.threadFactory);

        this.port = 5678;
    }

    /**
     * Returns the port number that the server is running on.
     * @return the port of the server.
     */
    public int getPort()
    {
        return port;
    }

    /**
     * Sets the port to run the server on.
     * @param port
     * @throws IOException
     */
    public void setPort(int port) throws IOException
    {
        this.port = port;
        this.serverSocket = new ServerSocket(port);
    }

    /**
     * Gets the number of threads in the server thread pool.
     * @return number of allotted threads.
     */
    public int getNumThreads()
    {
        return numThreads;
    }

    /**
     * Sets the number of threads in the server thread pool.
     * @param numThreads
     */
    public void setNumThreads(int numThreads)
    {
        this.numThreads = numThreads;
        this.executorService = Executors.newFixedThreadPool(this.numThreads,
                this.threadFactory);
    }

    /**
     * Runs the server with the specified port and number of threads.
     * This call is blocking, meaning it will run forever until interrupted.
     */
    public void run()
    {
        if (this.serverSocket == null)
            throw new IllegalArgumentException("Server socket was null. Set the port.");

        logger.info("Starting chat server on port: {}, threads: {}", this.port, this.numThreads);

        while (true)
        {
            try
            {
                Socket tempSocket = this.serverSocket.accept();
                ClientHandler ch = new ClientHandler(tempSocket);
                this.executorService.submit(ch);
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
                logger.error("Exception in server thread: {}", ex.getMessage());
            }
        }
    }
}
