package com.cs321.chat.server.interfaces;

import com.cs321.chat.core.exceptions.KeyNotFoundException;
import com.cs321.chat.core.exceptions.UsernameExistsException;
import com.cs321.chat.core.interfaces.ICheckinModel;
import com.cs321.chat.core.interfaces.IMessageModel;

import java.util.List;
import java.util.Queue;

public interface IThreadSafeModel
{
    public void addMessage(IMessageModel imm);
    public Queue<IMessageModel> getMessageQueue(String name) throws KeyNotFoundException;
    public IMessageModel getMessage(String name) throws KeyNotFoundException;
    public void checkIn(ICheckinModel cm) throws UsernameExistsException;
    public void checkOut(String username);
    public int getMessageQueueSize(String name) throws KeyNotFoundException;
    public List<String> getConnectedUsers();
}
