package com.cs321.chat.server;


import com.cs321.chat.core.exceptions.UsernameExistsException;
import com.cs321.chat.core.interfaces.ICheckinModel;
import com.cs321.chat.core.interfaces.IMessageModel;
import com.cs321.chat.server.interfaces.IThreadSafeModel;
import com.cs321.chat.core.exceptions.KeyNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

public class ThreadSafeModel implements IThreadSafeModel
{
    private Logger logger = LoggerFactory.getLogger(ThreadSafeModel.class);
    private static ThreadSafeModel ourInstance = new ThreadSafeModel();

    private Map<String, Queue<IMessageModel>> dataMap;   // Using concurrent hash map, and concurrent linked queue
                                                         // to add thread safety. The queue is lock-free, so no thread
                                                         // starvation. The map and queue should be size limited... meh
    private Queue<String> connectedUsers;

    private ThreadSafeModel()
    {
        logger.info("Instantiating the model.");
        this.dataMap = new ConcurrentHashMap<>();
        this.connectedUsers = new ConcurrentLinkedQueue<>();
    }

    /**
     * This is the singleton getInstance() method that returns the current instance of the singleton.
     * @return instance of the model.
     */
    public static ThreadSafeModel getInstance()
    {
        return ourInstance;
    }

    /**
     * This method adds a message to a mailbox. If the mailbox does not exist, it is created.
     * @param imm interfaced message model.
     */
    public void addMessage(IMessageModel imm)
    {
        String dst = imm.getDestination();
        if (!this.dataMap.containsKey(dst))
        {
            logger.debug("Destination mailbox: {} does not exist. Creating...", dst);
            Queue<IMessageModel> tempQ = new ConcurrentLinkedQueue<>();
            tempQ.add(imm);
            this.dataMap.put(dst, tempQ);
        }
        else
        {
            logger.trace("Destination mailbox exists.");
            this.dataMap.get(dst).add(imm); //Add the model to the queue.
        }
    }

    /**
     * This method returns the entire message queue that is held in the model for a username.
     * @param name the destination mailbox name
     * @return queue of interfaced message models.
     * @throws KeyNotFoundException
     */
    public Queue<IMessageModel> getMessageQueue(String name) throws KeyNotFoundException
    {
        if (!this.dataMap.containsKey(name))
            throw new KeyNotFoundException("The key: " + name + " did not exist.");

        return this.dataMap.get(name);
    }

    /**
     * This method polls the message queue for a destination. (gets and removes from queue)
     * @param name the destination mailbox name
     * @return an interfaced message model
     * @throws KeyNotFoundException
     */
    public IMessageModel getMessage(String name) throws KeyNotFoundException
    {
        if (!this.dataMap.containsKey(name))
            throw new KeyNotFoundException("The key: " + name + " did not exist.");

        return this.dataMap.get(name).poll();
    }

    /**
     * This method will add the user provided checkin model to the queue of active users.
     * @param cm checkin model
     */
    public void checkIn(ICheckinModel cm) throws UsernameExistsException
    {
        String uname = cm.getUsername();
        if (!this.connectedUsers.contains(uname))
        {

            this.connectedUsers.add(uname);

            if (!this.dataMap.containsKey(uname))
                this.dataMap.put(uname, new ConcurrentLinkedQueue<IMessageModel>());
        }
        else
            throw new UsernameExistsException("The user: " + cm.getUsername() + " already exists.");
    }

    /**
     * This method will remove a username from the list of active users.
     * @param username a username
     */
    public void checkOut(String username)
    {
        if (!this.connectedUsers.contains(username))
            return; //nothing to remove

        this.connectedUsers.remove(username); // Only remove the queue entry. Still want the messages.
    }

    /**
     * This method simply returns the size of the message queue for a specified user.
     * @param name destination mailbox name
     * @return the size of the message queue for the user
     * @throws KeyNotFoundException
     */
    public int getMessageQueueSize(String name) throws KeyNotFoundException
    {
        if (!this.dataMap.containsKey(name))
            throw new KeyNotFoundException("The key: " + name + " did not exist.");

        return this.dataMap.get(name).size();
    }

    /**
     * This method returns a list of the actively connected users.
     * @return list of connected users.
     */
    public List<String> getConnectedUsers()
    {
        return new ArrayList<>(this.connectedUsers);
    }
}
