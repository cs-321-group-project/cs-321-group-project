package com.cs321.chat.server;

import com.cs321.chat.core.ControlModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class ClientCommandHandler
{
    private final Logger logger = LoggerFactory.getLogger(ClientCommandHandler.class);

    private ThreadSafeModel modelInstance;
    private ControlModel controlModel;

    /**
     * Default constructor for the command handler.
     */
    public ClientCommandHandler()
    {
        this.init();
    }

    /**
     * Parameterized constructor of the command handler.
     * @param m
     */
    public ClientCommandHandler(ControlModel m)
    {
        this.init();
        this.controlModel = m;
    }

    private void init()
    {
        this.modelInstance = ThreadSafeModel.getInstance(); // Get the singleton instance.
    }

    /**
     * This method operates on a control model. It performs the command specified by the client, and sets the
     * output in the object. Once complete the object is returned with the output encapsulated.
     * @return filled control model.
     * @throws Exception
     */
    public ControlModel parseAndFill() throws Exception
    {
        if (this.controlModel == null)
            throw new Exception("Cannot parse the control model if its not set.");

        String command = this.controlModel.getCommand();
        switch (command)
        {
            case "list_users": // Populate the map with the output.
            {
                List<String> tempUsers = this.modelInstance.getConnectedUsers();
                this.controlModel.setCommandResult(command, tempUsers);
                break;
            }
            default:
            {
                logger.error("Command: '{}' is not recognised.", command);
                break;
            }
        }
        return this.controlModel;
    }
}
