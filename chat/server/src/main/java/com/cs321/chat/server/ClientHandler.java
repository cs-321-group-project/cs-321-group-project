package com.cs321.chat.server;

import com.cs321.chat.core.*;
import com.cs321.chat.core.exceptions.InvalidMessageException;
import com.cs321.chat.core.exceptions.InvalidUserNameException;
import com.cs321.chat.core.exceptions.KeyNotFoundException;
import com.cs321.chat.core.exceptions.UsernameExistsException;
import com.cs321.chat.core.interfaces.ICheckinModel;
import com.cs321.chat.core.interfaces.IMessageModel;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.util.Queue;
import java.util.concurrent.*;

public class ClientHandler implements Runnable
{
    private final Logger logger = LoggerFactory.getLogger(ClientHandler.class);
    private ThreadSafeModel model;
    private ScheduledExecutorService executorService;
    private ThreadFactory threadFactory;
    private Socket connection;
    private ObjectInputStream ois;
    private ObjectOutputStream oos;
    private String mailBox;
    private Queue<ControlModel> controlQueue;

    /**
     * Parameterized constructor for the client handler. It expects an active client socket to handle.
     * Once run via an executor service, the handler grabs the input and output streams of the socket
     * to perform i/o serialization for objects in core. It does this using a writing thread that responds
     * to the client using a polling methodology. The reading thread simply calls readObject() in a loop to
     * grab serialized objects from the client and place them in a model.
     * @param sock
     */
    public ClientHandler(Socket sock)
    {
        logger.debug("Instantiating the ClientHandler.");
        this.connection = sock;
        this.ois = null;
        this.oos = null;
        this.mailBox = null;
        this.model = ThreadSafeModel.getInstance();
        this.threadFactory = new ThreadFactoryBuilder()
                .setNameFormat("cli-thr-%d")
                .build();
        this.executorService = Executors.newScheduledThreadPool(1, this.threadFactory);
        this.controlQueue = new ConcurrentLinkedQueue<>();
    }

    private Runnable writer = new Runnable()
    {
        @Override
        public void run()
        {
            try
            {
                IMessageModel imm = model.getMessage(mailBox); // Poll the model queue
                ControlModel cm = controlQueue.poll(); // Get the head of the queue (if there) and remove

                if (connection.isClosed()) // If the connection is gone, we can't do anything anyways...
                    return;

                if (imm != null)
                {
                    logger.debug("Client: {}, has new mail: {}", mailBox,imm.getMsg());
                    oos.writeObject(imm);
                }
                if (cm != null)
                {
                    logger.debug("Client: {}, had a control model response waiting.", mailBox);
                    oos.writeObject(cm);
                }
            }
            catch (SocketException e)
            {
                logger.debug("Socket closed. Stopping client.");
            }
            catch (IOException e)
            {
                logger.error("Caught exception in writer: {}", e.getMessage());
            }
            catch (KeyNotFoundException e)
            {
                logger.error("The mailbox did not exist: {} ", e.getMessage());
            }
        }
    };

    @Override
    public void run() // From Runnable
    {
        try
        {
            logger.debug("Handling client: {}", connection.toString());
            ois = new ObjectInputStream(connection.getInputStream());
            oos = new ObjectOutputStream(connection.getOutputStream());

            ICheckinModel cm = null;
            Object model = ois.readObject();

            if (model instanceof ICheckinModel) // Are we a checkin?
            {
                cm = (ICheckinModel) model;
                this.model.checkIn(cm);
                logger.trace("Checking in.");
            }
            else
            {
                throw new InvalidUserNameException("The client did not checkin.");
            }
            this.mailBox = cm.getUsername();
            // This executor service polls on an interval instead of the horrible while(true) i had:
            this.executorService.scheduleAtFixedRate(writer, 0, 100, TimeUnit.MILLISECONDS);
            logger.trace("Submitted executor.");
            IMessageModel mm = null;
            Object obj = ois.readObject();
            ControlModel con_m = null;

            while (obj != null)
            {
                if (obj instanceof IMessageModel) // Are we a message?
                {
                    mm = (IMessageModel) obj;
                    logger.debug("Got data: {}", mm);
                    this.model.addMessage(mm);
                }
                else if (obj instanceof ControlModel) // Are we a ControlModel?
                {
                    con_m = (ControlModel) obj;
                    ClientCommandHandler cch = new ClientCommandHandler(con_m);
                    this.controlQueue.add(cch.parseAndFill());
                }
                else // I dont know what we are!
                {
                    throw new InvalidMessageException("Data provided was not in the message model format.");
                }
                obj = ois.readObject(); // Read again.
            }
        }
        catch (EOFException eofex) // To catch when the readObject() has no more input.
        {
            logger.debug("End of stream reached.");
            logger.debug("Client disconnect: {}", connection.toString());
        }
        catch (SocketException ex)
        {
            logger.debug("Client closed the socket.");
        }
        catch (InvalidMessageException ex)
        {
            ex.printStackTrace();
            logger.error("Invalid Message Exception in client handler: {}, closing stream.", ex.getMessage());
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
            logger.error("Input Stream Exception in client handler: {}, closing stream.", ex.getMessage());
        }
        catch (InvalidUserNameException ex)
        {
            logger.error("User Name Exception in client handler: {}, closing stream.", ex.getMessage());
        }
        catch (UsernameExistsException ex)
        {
            logger.error("User {} already exists.", this.mailBox);
        }
        catch (ClassNotFoundException ex) // in read object
        {
            logger.error("Class not Found Exception in client handler: {}, closing stream.", ex.getMessage());
        }
        catch (Exception e) // Unhandled exception
        {
            e.printStackTrace();
            logger.error("Unhandled Exception: {}", e.getMessage());
        }
        finally // Clean up
        {
            logger.trace("Finally closing streams...");
            logger.debug("Stopping client: {}.", this.mailBox);
            this.model.checkOut(this.mailBox); // Remove from the queue of active users.
            // Close the streams first... dummy
            if (ois != null)
                try { ois.close(); } catch (Exception e) {  } // Just swallow the exception
            if (oos != null)
                try { oos.close(); } catch (Exception e) { }
            // Now close the connection
            if (connection != null)
                try { connection.close(); } catch (Exception e) {  }
            // Stop the executor service (if its still going)
            if (!executorService.isShutdown())
                executorService.shutdown();
        }
    }
}
