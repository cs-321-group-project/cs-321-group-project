package com.cs321.chat.server;

import java.io.IOException;
import java.util.Arrays;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

/**
 * Server application for CS321 group project.
 *
 */
public class App 
{
    private static final Logger logger = LoggerFactory.getLogger(App.class);

    /**
     * Entry point for the server application.
     * @param args
     */
    public static void main( String[] args )
    {
        try
        {
            ChatServer cs;
            logger.debug("Args: {}", Arrays.toString(args));
            if (args.length == 0 || args.length > 2)
            {
                logger.info("Empty or incorrect commandline arguments. Applying defaults. \n\t " +
                        "Usage: java -jar <server.jar> <port> <n_threads>.");
                cs = new ChatServer();
                cs.setPort(3456);
                cs.setNumThreads(40);
                cs.run();

                return;
            }
            int port = Integer.parseInt(args[0]);
            int numThreads = Integer.parseInt(args[1]);

            cs = new ChatServer();
            cs.setPort(port);
            cs.setNumThreads(numThreads);
            cs.run();
        }
        catch (NumberFormatException nfe)
        {
            logger.error("Cannot parse integers from commandline input: {}", nfe.getMessage());
        }
        catch (IOException e)
        {
            logger.error("Encountered exception while running: {}", e.getMessage());
        }
        catch (Exception e)
        {
            e.printStackTrace();
            logger.error("Unhandled exception: {}", e.getMessage());
        }
    }
}
