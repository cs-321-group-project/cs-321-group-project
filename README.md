# CS321 Chat Client Project
### Authors: 
- Adam Dumbacher @adamdumbacher
- Haley Delmas @hed0004
- Evan Jaramillo @javamn
- Evan Swinney @evan.swinney

### Description:
This project is a simple Java based chat client developed for our CS321 class at the University of Alabama in Huntsville.

# Maven Build System

## Project Structure:
```
               +----------+
               |          |
      +--------+   Core   +---------+
      |        |          |         |
      |        +----------+         |
      |                             |
      |                             |
      |                             |
+-----+------+                +-----+------+
|            |                |            |
|   Server   |                |   Client   |
|            |                |            |
+------------+                +------------+
```

## How to build:
- To build the project, clone it and chage directory into chat.
- Run `mvn clean install` this will install each sub-module to the .m2/ directory and build executable .jar files for the server and client sub-modules.
- To run a either server or client, change directory into `chat/server/target` or `chat/client/target` and run `java -jar server-1.0-SNAPSHOT.jar` or `java -jar client-1.0-SNAPSHOT.jar`
- Note that core does not build an executable .jar, but we will use core to hold classes that both server and client have in common. To build either the server or the client module, core must first be installed via `mvn install` in the core directory.